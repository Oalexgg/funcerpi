// Requires
const config = require("./config.js");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const server = require("http").createServer(app);
const path = require("path");
const dir = path.join(__dirname) + "/resources/static/assets/uploads/";

app.use(cors());
app.use(
  bodyParser.json({
    limit: "100mb",
  })
);
app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    extended: true,
  })
);

app.use("/images", express.static(dir));

//Routes
const login_routes = require("./routes/login.routes");
const cliente_routes = require("./routes/cliente.routes");
const compra_routes = require("./routes/compra.routes");
const empresa_routes = require("./routes/empresa.routes");
const producto_routes = require("./routes/producto.routes");
const usertypes_routes = require("./routes/user-type.routes");
const cupon_routes = require("./routes/cupon.routes");
const upload_routes = require("./routes/upload.routes");

//Login routes
app.use("/api", login_routes);

//Common routes
app.use("/api", cliente_routes);
app.use("/api", compra_routes);
app.use("/api", empresa_routes);
app.use("/api", producto_routes);
app.use("/api", usertypes_routes);
app.use("/api", cupon_routes);

// Upload routes
app.use("/api", upload_routes);

/**
 * Cambiar esto por 0.0.0.0
 */
// server.listen(config.port, "192.168.100.3", () => {
server.listen(config.port, () => {
  console.log(
    `Example app listening at http://${server.address().address}:${config.port}`
  );
  // MongoDB
  mongoose.connect(
    "mongodb://localhost:27017/" + config.database,
    {
      useNewUrlParser: true,
    },
    (err) => {
      if (err) throw err;
      console.log("MongoDB: \x1b[32m%s\x1b[0m", "Online");
    }
  );
});
