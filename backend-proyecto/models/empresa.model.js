const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EmpresaSchema = Schema({
  nombre: String,
  calle: String,
  colonia: String,
  codigo_postal: String,
  rfc: { type: String, unique: true },
  telefono: Number,
});

module.exports = mongoose.model("Empresa", EmpresaSchema);
