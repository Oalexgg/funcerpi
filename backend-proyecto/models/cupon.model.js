const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CuponSchema = Schema({
  descripcion: String,
  codigo: { type: String, unique: true },
  estado: Boolean,
  expiracion: Date,
});

module.exports = mongoose.model("Cupon", CuponSchema);
