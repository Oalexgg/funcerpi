const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;

const ClienteSchema = Schema({
  nombre: String,
  apellido: String,
  email: String,
  password: String,
  empresa: { type: Schema.Types.ObjectId, ref: "Empresa" },
  tipo: { type: Schema.Types.ObjectId, ref: "UserType" },
});

ClienteSchema.pre("save", function (next) {
  var cliente = this;

  // only hash the password if it has been modified (or is new)
  if (!cliente.isModified("password")) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(cliente.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      cliente.password = hash;
      next();
    });
  });
});

ClienteSchema.methods.comparePassword = async function (candidatePassword) {
  return await bcrypt.compare(candidatePassword, this.password);
};

module.exports = mongoose.model("Cliente", ClienteSchema);
