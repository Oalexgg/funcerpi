const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserTypesSchema = Schema({
  nombre: String,
  permisos: [
    {
      text: String,
      route: String,
      icon: String,
      permiso: Boolean,
    },
  ],
});

module.exports = mongoose.model("UserType", UserTypesSchema);
