const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CompraSchema = Schema({
  fecha: {
    type: String,
    default: new Date(),
  },
  total: Number,
  subtotal: Number,
  impuestos: Number,
  productos: [
    {
      producto: {
        type: Schema.Types.ObjectId,
        ref: "Producto",
      },
      medida: String,
      cantidad: Number,
    },
  ],
  cupon: { type: Schema.Types.ObjectId, ref: "Cupon" },
  cliente: { type: Schema.Types.ObjectId, ref: "Cliente" },
  estado: {
    type: String,
    enum: ["No revisado", "Revisado", "Validación", "Propuesta"],
    default: "No revisado",
  },
});

module.exports = mongoose.model("Compra", CompraSchema);
