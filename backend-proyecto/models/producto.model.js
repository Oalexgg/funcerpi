const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductoSchema = Schema({
  nombre: String,
  descripcion: String,
  precio: Number,
  img: { type: String, default: "" },
});
module.exports = mongoose.model("Producto", ProductoSchema);
