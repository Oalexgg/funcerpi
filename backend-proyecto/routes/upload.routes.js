const express = require("express");
const uploadController = require("./../controllers/upload.controller");

const api = express.Router();

api.post(
  "/upload",
  uploadController.uploadImage
);

module.exports = api;
