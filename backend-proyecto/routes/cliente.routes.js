const express = require("express");
const clienteController = require("./../controllers/cliente.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get(
  "/cliente/:id?",
  [param("id").isString()],
  clienteController.listClientes
);
api.post(
  "/cliente",
  [
    body("nombre").isString().notEmpty(),
    body("apellido").isString().notEmpty(),
    body("email").normalizeEmail().isEmail().notEmpty(),
    body("password").isString().notEmpty(),
    body("companyname").isString().notEmpty(),
    body("calle").isString().notEmpty(),
    body("colonia").isString().notEmpty(),
    body("codigo_postal").isNumeric().notEmpty(),
    body("rfc").isString().notEmpty(),
    body("telefono").isString().notEmpty(),
    body("tycs").isBoolean().notEmpty(),
  ],
  clienteController.saveCliente
);
api.put(
  "/cliente/:id",
  [
    param("id").isMongoId().notEmpty(),
    body("nombre").isString().notEmpty(),
    body("apellido").isString().notEmpty(),
    body("email").normalizeEmail().isEmail().notEmpty(),
    body("password").isString().notEmpty(),
    body("companyname").isString().notEmpty(),
    body("calle").isString().notEmpty(),
    body("colonia").isString().notEmpty(),
    body("codigo_postal").isNumeric().notEmpty(),
    body("rfc").isString().notEmpty(),
    body("telefono").isNumeric().notEmpty(),
  ],
  clienteController.updateCliente
);
api.delete(
  "/cliente/:id",
  [param("id").isMongoId()],
  clienteController.deleteCliente
);
module.exports = api;
