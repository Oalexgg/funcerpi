const express = require("express");
const loginController = require("./../controllers/login.controller");
const { body, param, query } = require("express-validator");

const api = express.Router();

api.post(
  "/login",
  [
    body("email").normalizeEmail().isEmail().notEmpty(),
    body("password").isString().notEmpty(),
  ],
  loginController.login
);
api.post(
  "/logout",
  [param("id").isMongoId().notEmpty()],
  loginController.logout
);

api.get("/init", [query("token").isString().notEmpty()], loginController.init);

module.exports = api;
