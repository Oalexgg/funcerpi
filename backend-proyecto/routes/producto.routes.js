const express = require("express");
const productoController = require("./../controllers/producto.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get(
  "/producto/:id?",
  [param("id").isMongoId()],
  productoController.listProductos
);
api.post(
  "/producto",
  [
    body("nombre").isString().notEmpty(),
    body("descripcion").isString().notEmpty(),
    body("precio").isNumeric().notEmpty(),
    body("img").isString().optional(),
  ],
  productoController.saveProducto
);

api.put(
  "/producto/:id",
  [
    param("id").isMongoId().notEmpty(),
    body("nombre").isString().notEmpty(),
    body("descripcion").isString().notEmpty(),
    body("precio").isNumeric().notEmpty(),
    body("img").isString().optional(),
  ],
  productoController.updateProducto
);
api.delete(
  "/producto/:id",
  [param("id").isMongoId().notEmpty()],
  productoController.deleteProducto
);
module.exports = api;
