const express = require("express");
const empresaController = require("./../controllers/empresa.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get(
  "/empresa/:id?",
  [param("id").isMongoId()],
  empresaController.listEmpresas
);
api.post(
  "/empresa",
  [
    body("nombre").isString().notEmpty(),
    body("calle").isString().notEmpty(),
    body("colonia").isString().notEmpty(),
    body("rfc").isString().notEmpty(),
    body("telefono").isNumeric().notEmpty(),
  ],
  empresaController.saveEmpresa
);
api.put(
  "/empresa/:id",
  [
    param("id").isMongoId(),
    body("nombre").isString(),
    body("calle").isString(),
    body("colonia").isString(),
    body("rfc").isString(),
    body("telefono").isNumeric(),
  ],
  empresaController.updateEmpresa
);
api.delete(
  "/empresa/:id",
  [param("id").isMongoId()],
  empresaController.deleteEmpresa
);
module.exports = api;
