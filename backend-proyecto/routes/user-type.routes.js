const express = require("express");
const userTypeController = require("./../controllers/user-type.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get(
  "/usertype/:id?",
  [param("id").isString()],
  userTypeController.listUserTypes
);
api.post(
  "/usertype",
  [
    body("nombre").isString(),
    body("permisos").isArray(),
  ],
  userTypeController.saveUserType
);
api.put(
  "/usertype/:id",
  [
    param("id").isMongoId(),
    body("permisos").isArray(),
  ],
  userTypeController.updateUserType
);
api.delete(
  "/usertype/:id",
  [param("id").isMongoId()],
  userTypeController.deleteUserType
);
module.exports = api;
