const express = require("express");
const cuponController = require("./../controllers/cupon.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get("/cupon/:id?", [param("id").isString()], cuponController.listCupons);
api.post(
  "/cupon",
  [
    body("descripcion").isString().notEmpty(),
    body("codigo").isString().notEmpty(),
    body("estado").isBoolean().notEmpty(),
    body("expiracion").isString().notEmpty(),
  ],
  cuponController.saveCupon
);
api.put(
  "/cupon/:id",
  [
    param("id").isMongoId().notEmpty(),
    body("descripcion").isString().notEmpty(),
    body("codigo").isString().notEmpty(),
    body("estado").isBoolean().notEmpty(),
    body("expiracion").isString().notEmpty(),
  ],
  cuponController.updateCupon
);
api.delete(
  "/cupon/:id",
  [param("id").isMongoId().notEmpty()],
  cuponController.deleteCupon
);
module.exports = api;
