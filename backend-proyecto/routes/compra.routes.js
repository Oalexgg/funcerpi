const express = require("express");
const compraController = require("./../controllers/compra.controller");
const { body, param } = require("express-validator");

const api = express.Router();

api.get("/pedidos/:id?", [param("id").isString()], compraController.listCompras);
api.post(
  "/pedidos",
  [
    body("total").isNumeric().notEmpty(),
    body("subtotal").isNumeric().notEmpty(),
    body("impuestos").isNumeric().notEmpty(),
    body("productos").isArray().notEmpty(),
    body("cupon").isMongoId().optional(),
    body("cliente").isMongoId().notEmpty(),
  ],
  compraController.saveCompra
);
api.put(
  "/pedidos/:id",
  [
    param("id").isMongoId().notEmpty(),
    body("total").isNumeric().notEmpty(),
    body("subtotal").isNumeric().notEmpty(),
    body("impuestos").isNumeric().notEmpty(),
    body("productos").isArray().notEmpty(),
    body("cupon").isMongoId(),
    body("cliente").isMongoId().notEmpty(),
  ],
  compraController.updateCompra
);
api.delete(
  "/pedidos/:id",
  [param("id").isMongoId().notEmpty()],
  compraController.deleteCompra
);
module.exports = api;
