const Cliente = require("./../models/cliente.model");
const Empresa = require("./../models/empresa.model");
const UserType = require("./../models/user-type.model");
const { validationResult } = require("express-validator");

function listClientes(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = Cliente.findById(id);
  } else {
    query = Cliente.find();
  }

  query
    .populate("empresa")
    .populate("tipo")
    .exec((err, cliente) => {
      if (err) {
        res.status(500).send({
          message: "Error en la petición",
        });
      } else {
        if (
          (!Array.isArray(cliente) && !cliente) ||
          (Array.isArray(cliente) && !cliente.length)
        ) {
          res.status(404).send({
            message: "No se encontraron clientes",
          });
        } else {
          res.status(200).send({
            cliente,
          });
        }
      }
    });
}

async function saveCliente(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;

  var empresa = await Empresa.findOne({ rfc: params.rfc });

  if (!empresa) {
    empresa = new Empresa({ ...params, name: params.companyname });
    try {
      empresa = await empresa.save();
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: "Ocurrió un error al guardar la empresa",
      });
    }
  }

  var usertype_object = {
    nombre: "cliente",
    permisos: [
      {
        text: "Panel de control",
        route: "/admin/dashboard",
        icon: "dashboard",
        permiso: true,
      },
      {
        text: "Productos",
        route: "/admin/productos",
        icon: "login",
        permiso: true,
      },
    ],
  };
  var usertype = await UserType.findOne({ nombre: usertype_object.nombre });

  if (!usertype) {
    usertype = new UserType(usertype_object);
    try {
      usertype = await usertype.save();
    } catch (error) {
      console.log(error);
      return res.status(500).send({
        message: "Ocurrió un error al guardar el tipo de usuario",
      });
    }
  }

  var cliente_object = ({ nombre, apellido, email, password } = params);
  cliente_object.empresa = empresa._id;
  cliente_object.tipo = usertype._id;

  var cliente = new Cliente({ ...cliente_object });

  cliente.save((err, clienteStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el cliente",
      });
    } else {
      if (!clienteStored) {
        res.status(404).send({
          message: "No se ha guardado el cliente",
        });
      } else {
        res.status(200).send({
          cliente: clienteStored,
        });
      }
    }
  });
}

async function updateCliente(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  var empresa_object = ({
    nombre: companyname,
    rfc,
    calle,
    colonia,
    codigo_postal,
    telefono,
  } = update);
  const updateCliente = ({ nombre, apellido, email, password, tipo } = update);
  if (updateCliente.password === "******") {
    delete updateCliente.password;
  }
  var empresa = await Empresa.findOne({ rfc: empresa_object.rfc });

  empresa.nombre = empresa_object.companyname;
  empresa.rfc = empresa_object.rfc;
  empresa.calle = empresa_object.calle;
  empresa.colonia = empresa_object.colonia;
  empresa.codigo_postal = empresa_object.codigo_postal;
  empresa.telefono = empresa_object.telefono;

  try {
    empresa = await empresa.save();
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Ocurrió un error al guardar la empresa",
    });
  }

  Cliente.findByIdAndUpdate(id, updateCliente, (err, clienteUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!clienteUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el cliente",
      });
    }
    res.status(200).send({
      cliente: clienteUpdated,
    });
  });
}

function deleteCliente(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  Cliente.findByIdAndRemove(id, (err, clienteRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!clienteRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el cliente",
      });
    }
    res.status(200).send({
      cliente: clienteRemoved,
    });
  });
}

module.exports = {
  listClientes,
  saveCliente,
  updateCliente,
  deleteCliente,
};
