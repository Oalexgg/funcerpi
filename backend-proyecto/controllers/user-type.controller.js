const UserType = require("./../models/user-type.model");
const { validationResult } = require("express-validator");

function listUserTypes(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = UserType.findById(id);
  } else {
    query = UserType.find();
  }

  query.exec((err, usertype) => {
    if (err) {
      res.status(500).send({
        message: "Error en la petición",
      });
    } else {
      if (
        (!Array.isArray(usertype) && !usertype) ||
        (Array.isArray(usertype) && !usertype.length)
      ) {
        res.status(404).send({
          message: "No se encontraron tipos de usuario",
        });
      } else {
        res.status(200).send({
          usertype,
        });
      }
    }
  });
}

function saveUserType(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;
  var usertype = new UserType({ ...params });

  usertype.save((err, usertypeStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el usertype",
      });
    } else {
      if (!usertypeStored) {
        res.status(404).send({
          message: "No se ha guardado el usertype",
        });
      } else {
        res.status(200).send({
          usertype: usertypeStored,
        });
      }
    }
  });
}

function updateUserType(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  UserType.findByIdAndUpdate(id, update, (err, usertypeUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!usertypeUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el usertype",
      });
    }
    res.status(200).send({
      usertype: usertypeUpdated,
    });
  });
}

function deleteUserType(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  UserType.findByIdAndRemove(id, (err, usertypeRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!usertypeRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el usertype",
      });
    }
    res.status(200).send({
      usertype: usertypeRemoved,
    });
  });
}

module.exports = {
  listUserTypes,
  saveUserType,
  updateUserType,
  deleteUserType,
};
