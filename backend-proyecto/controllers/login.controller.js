const Cliente = require("./../models/cliente.model");
const Empresa = require("./../models/empresa.model");
const UserType = require("./../models/user-type.model");

const { validationResult } = require("express-validator");
const moment = require("moment");

async function login(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const { email, password } = req.body;
  const cliente = await Cliente.findOne({ email }).populate("tipo");

  if (!cliente || !(await cliente.comparePassword(password))) {
    return res.status(401).send({
      message: `Usuario o contraseña incorrecta, por favor revise los datos ingresados.`,
    });
  }
  delete cliente._doc.password;
  res.status(200).send({
    message: "Login correcto",
    cliente,
  });
}

async function logout(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  res.sendStatus(200);
}

async function init(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }

  if (req.query.token !== "func3rp1") {
    return res.status(422).send({
      message: "Token incorrecto, verifíquelo.",
    });
  }

  const tipo = await UserType.findOne({ nombre: "admin" });
  if (tipo) {
    return res.status(422).send({
      message:
        "El usuario ya ha sido creado, contacte al administrador para ayuda.",
    });
  }
  var empresa_object = {
    nombre: "FUNCERPI",
    rfc: "",
    calle: "",
    colonia: "",
    codigo_postal: "",
    telefono: "",
  };
  var empresa = new Empresa(empresa_object);
  try {
    empresa = await empresa.save();
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Ocurrió un error al guardar la empresa",
    });
  }

  var usertype_object = {
    nombre: "admin",
    permisos: [
      {
        text: "Panel de control",
        route: "/admin/dashboard",
        icon: "dashboard",
        permiso: true,
      },
      {
        text: "Productos",
        route: "/admin/productos",
        icon: "login",
        permiso: true,
      },
    ],
  };
  var usertype = new UserType(usertype_object);
  try {
    usertype = await usertype.save();
  } catch (error) {
    console.log(error);
    return res.status(500).send({
      message: "Ocurrió un error al guardar el tipo de usuario",
    });
  }

  var cliente_object = {
    nombre: "Administrador",
    apellido: "",
    email: "admin@admin.com",
    password: "123456",
  };
  cliente_object.empresa = empresa._id;
  cliente_object.tipo = usertype._id;

  var cliente = new Cliente({ ...cliente_object });

  cliente.save((err, clienteStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el cliente",
      });
    } else {
      if (!clienteStored) {
        res.status(404).send({
          message: "No se ha guardado el cliente",
        });
      } else {
        res.status(200).send({
          cliente: clienteStored,
        });
      }
    }
  });
}

module.exports = {
  login,
  logout,
  init,
};
