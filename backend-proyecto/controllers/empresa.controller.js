const Empresa = require("./../models/empresa.model");
const { validationResult } = require("express-validator");

function listEmpresas(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = Empresa.findById(id);
  } else {
    query = Empresa.find();
  }

  query.exec((err, empresa) => {
    if (err) {
      res.status(500).send({
        message: "Error en la petición",
      });
    } else {
      if (
        (!Array.isArray(empresa) && !empresa) ||
        (Array.isArray(empresa) && !empresa.length)
      ) {
        res.status(404).send({
          message: "No se encontraron activos",
        });
      } else {
        res.status(200).send({
          empresa,
        });
      }
    }
  });
}

function saveEmpresa(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;
  var empresa = new Empresa({ ...params });

  empresa.save((err, empresaStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el empresa",
      });
    } else {
      if (!empresaStored) {
        res.status(404).send({
          message: "No se ha guardado el empresa",
        });
      } else {
        res.status(200).send({
          empresa: empresaStored,
        });
      }
    }
  });
}

function updateEmpresa(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  Empresa.findByIdAndUpdate(id, update, (err, empresaUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!empresaUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el empresa",
      });
    }
    res.status(200).send({
      empresa: empresaUpdated,
    });
  });
}

function deleteEmpresa(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  Empresa.findByIdAndRemove(id, (err, empresaRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!empresaRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el empresa",
      });
    }
    res.status(200).send({
      empresa: empresaRemoved,
    });
  });
}

module.exports = {
  listEmpresas,
  saveEmpresa,
  updateEmpresa,
  deleteEmpresa,
};
