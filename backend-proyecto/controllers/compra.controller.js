const Compra = require("./../models/compra.model");
const { validationResult } = require("express-validator");

function listCompras(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = Compra.findById(id);
  } else {
    query = Compra.find();
  }

  query
    .populate("productos.producto")
    .populate("cupon")
    .populate("cliente")
    .exec((err, pedido) => {
      if (err) {
        res.status(500).send({
          message: "Error en la petición",
        });
      } else {
        if (
          (!Array.isArray(pedido) && !pedido) ||
          (Array.isArray(pedido) && !pedido.length)
        ) {
          res.status(404).send({
            message: "No se encontraron pedidos",
          });
        } else {
          res.status(200).send({
            pedido,
          });
        }
      }
    });
}

function saveCompra(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;
  var compra = new Compra({ ...params });

  compra.save((err, compraStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el pedido",
      });
    } else {
      if (!compraStored) {
        res.status(404).send({
          message: "No se ha guardado el pedido",
        });
      } else {
        res.status(200).send({
          pedido: compraStored,
        });
      }
    }
  });
}

function updateCompra(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  Compra.findByIdAndUpdate(id, update, (err, compraUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!compraUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el pedido",
      });
    }
    res.status(200).send({
      pedido: compraUpdated,
    });
  });
}

function deleteCompra(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  Compra.findByIdAndRemove(id, (err, compraRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!compraRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el pedido",
      });
    }
    res.status(200).send({
      pedido: compraRemoved,
    });
  });
}

module.exports = {
  listCompras,
  saveCompra,
  updateCompra,
  deleteCompra,
};
