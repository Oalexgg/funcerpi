const Cupon = require("./../models/cupon.model");
const { validationResult } = require("express-validator");

function listCupons(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = Cupon.findById(id);
  } else {
    query = Cupon.find();
  }

  query.exec((err, cupon) => {
    if (err) {
      res.status(500).send({
        message: "Error en la petición",
      });   
    } else {
      if (
        (!Array.isArray(cupon) && !cupon) ||
        (Array.isArray(cupon) && !cupon.length)
      ) {
        res.status(404).send({
          message: "No se encontraron activos",
        });
      } else {
        res.status(200).send({
          cupon,
        });
      }
    }
  });
}

function saveCupon(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;
  var cupon = new Cupon({ ...params });

  cupon.save((err, cuponStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el cupon",
      });
    } else {
      if (!cuponStored) {
        res.status(404).send({
          message: "No se ha guardado el cupon",
        });
      } else {
        res.status(200).send({
          cupon: cuponStored,
        });
      }
    }
  });
}

function updateCupon(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  Cupon.findByIdAndUpdate(id, update, (err, cuponUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!cuponUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el cupon",
      });
    }
    res.status(200).send({
      cupon: cuponUpdated,
    });
  });
}

function deleteCupon(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  Cupon.findByIdAndRemove(id, (err, cuponRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!cuponRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el cupon",
      });
    }
    res.status(200).send({
      cupon: cuponRemoved,
    });
  });
}

module.exports = {
  listCupons,
  saveCupon,
  updateCupon,
  deleteCupon,
};
