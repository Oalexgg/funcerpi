const Producto = require("./../models/producto.model");
const { validationResult } = require("express-validator");
const path = require("path");
const fs = require("fs");
const dir = path.join(__dirname, "../") + "/resources/static/assets/uploads/";

function listProductos(req, res) {
  const id = req.params.id;
  let query;
  if (id) {
    query = Producto.findById(id);
  } else {
    query = Producto.find();
  }

  query.exec((err, producto) => {
    if (err) {
      res.status(500).send({
        message: "Error en la petición",
      });
    } else {
      if (
        (!Array.isArray(producto) && !producto) ||
        (Array.isArray(producto) && !producto.length)
      ) {
        res.status(404).send({
          message: "No se encontraron activos",
        });
      } else {
        res.status(200).send({
          producto,
        });
      }
    }
  });
}

function saveProducto(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var params = req.body;
  var producto = new Producto({ ...params });

  producto.save((err, productoStored) => {
    if (err) {
      console.log(err);
      res.status(500).send({
        message: "Error al guardar el producto",
      });
    } else {
      if (!productoStored) {
        res.status(404).send({
          message: "No se ha guardado el producto",
        });
      } else {
        res.status(200).send({
          producto: productoStored,
        });
      }
    }
  });
}

async function updateProducto(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  const id = req.params.id;
  const update = req.body;
  const prod = await Producto.findById(id);
  if (prod.img && prod.img !== update.img) {
    if (fs.existsSync(`${dir}${prod.img}`)) fs.unlinkSync(`${dir}${prod.img}`);
  }
    Producto.findByIdAndUpdate(id, update, (err, productoUpdated) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de update ",
      });
    } else if (!productoUpdated) {
      return res.status(500).send({
        message: "No se ha actualizado el producto",
      });
    }
    res.status(200).send({
      producto: productoUpdated,
    });
  });
}

function deleteProducto(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(404).json({ errors: errors.array() });
  }
  var id = req.params.id;
  Producto.findByIdAndRemove(id, (err, productoRemoved) => {
    if (err) {
      return res.status(500).send({
        message: "Error en la peticion de delete ",
      });
    } else if (!productoRemoved) {
      return res.status(500).send({
        message: "No se ha eliminado el producto",
      });
    }
    res.status(200).send({
      producto: productoRemoved,
    });
  });
}

module.exports = {
  listProductos,
  saveProducto,
  updateProducto,
  deleteProducto,
};
