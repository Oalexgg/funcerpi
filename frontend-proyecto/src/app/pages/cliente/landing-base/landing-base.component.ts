import { DOCUMENT } from '@angular/common';
import { Component, HostListener, Inject, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-base',
  templateUrl: './landing-base.component.html',
  styleUrls: ['./landing-base.component.scss'],
})
export class LandingBaseComponent implements OnInit {
  @Input('titulo') titulo: string;
  @Input('fondo') fondo: string  = 'fondo="./../../../../assets/img/fabrica.jpg"';

  constructor(@Inject(DOCUMENT) private document: Document) {}
  logo = 'logo-white';
  active = false;
  scrolled = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (
      document.body.scrollTop
        ? document.body.scrollTop < 150
        : document.documentElement.scrollTop < 150
    ) {
      this.scrolled = false;
      if (this.active) return; //bug
      document.getElementById('navbar').classList.add('transparent');
      document.getElementById('navbar').classList.remove('scrolled');
      this.logo = 'logo-white';
    }

    if (
      document.body.scrollTop
        ? document.body.scrollTop > 150
        : document.documentElement.scrollTop > 150
    ) {
      document.getElementById('navbar').classList.add('scrolled');
      document.getElementById('navbar').classList.remove('transparent');
      this.logo = 'logo';
      this.scrolled = true;
    }
     
  }

  ngOnInit(): void {
    
  }

  showOptions(): void {
    if (this.scrolled && this.active) return;
    this.active = !this.active;
    if (this.active) {
      if (!document.getElementById('navbar').classList.contains('scrolled')) {
        document.getElementById('navbar').classList.add('scrolled');
        document.getElementById('navbar').classList.remove('transparent');
      }
    } else {
      if (
        !document.getElementById('navbar').classList.contains('transparent')
      ) {
        document.getElementById('navbar').classList.add('transparent');
        document.getElementById('navbar').classList.remove('scrolled');
      }
    }
    this.logo = this.active ? 'logo' : 'logo-white';
  }
}
