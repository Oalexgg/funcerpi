import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { DeviceDetectorService } from 'ngx-device-detector';

import { AuthService } from 'src/app/services/auth/auth.service';
import { StorageService } from 'src/app/services/storage/storage.service';

import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  isMobile = false;

  constructor(
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private   storageService: StorageService,
    private deviceService: DeviceDetectorService,
    public modal: MatDialog
  ) {
    this.isMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.setupForm();
  }

  setupForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(6), Validators.required]],
      remember: [true],
    });
  }

  login(): void {
    this.submitted = true;
    this._authService
      .login({
        email: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value,
      })
      .subscribe(
        (res) => {
          const { cliente } = res;
          if (cliente) {
            const remember = this.loginForm.get('remember').value;
            if (remember) {
              this.storageService.setItem('user', JSON.stringify(cliente));
            } else {
              this._authService.isLoggedInAdmin =
                cliente.tipo?.nombre === 'admin';
              this._authService.isLoggedInUser =
                cliente.tipo?.nombre === 'cliente';
              this._authService.cliente = cliente;
            }
            this.showSnackbar('Sesión iniciada correctamente.', 'Aceptar', {
              duration: 2000,
            });
            console.log(this._authService.isLoggedInAdmin)
            console.log(cliente.tipo?.nombre)
            console.log(this._authService.isLoggedInUser)
            console.log(cliente.tipo?.nombre)
            console.log(this._authService.cliente)
          //  this.router.navigateByUrl('/admin');
          } else {
            this.showSnackbar(
              '¡OOPS! ocurrió un error al iniciar sesión.',
              'Aceptar',
              {
                duration: 2000,
              }
            );
          }
        },
        (err) => console.log(err)
      );
  }

  showSnackbar(message: string, action: string, options: Object): void {
    this._snackBar.open(message, action, options);
  }

  abrirRegistro(): any {
    return this.modal.open(RegisterComponent, {
      width: this.isMobile ? '100vw' : '60vw',
      data: 'Registro al sistema',
    });
  }
}
