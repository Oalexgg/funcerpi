import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  constructor() {}

  cards = [
    {
      image: 'mision',
      title: 'Misión',
      content: [
        'Fabricar productos de buena calidad y durabilidad para los clientes que depositan su confianza en nosotros.',
      ],
    },
    {
      image: 'vision',
      title: 'Visión',
      content: [
        'Ser la empresa regional, mejor reconocida por nuestros clientes, y con la mejor calidad de nuestros productos.',
      ],
    },
    {
      image: 'valores',
      title: 'Valores',
      content: [
      'Trabajamos con un amplio Sentido de Negocio, agregando valor a todo lo que hacemos.', 
      'A través del Trabajo en Equipo mejoramos nuestros resultados individuales y de la compañía.',
      'Mantenemos una buena Calidad de Vida que nos permite crecer y desarrollar mejor nuestras actividades.',
      'Comunicación y Compromiso son elementos fundamentales en nuestra cultura de trabajo.',
      ],
    },
  ];

  ngOnInit(): void {}
}
