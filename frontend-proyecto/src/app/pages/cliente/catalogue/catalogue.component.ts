import { Component, OnInit } from '@angular/core';
import { CRUDService } from 'src/app/services/crud/crud.service';
import { image_url } from 'src/app/shared/constants';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
})
export class CatalogueComponent implements OnInit {
  productos = [];
  shoppingList = [];
  sizeProduct : string [] = [];
  public readonly SERVER_URL = image_url;
  constructor(
    private _productosService: CRUDService,
    private _snackBar: MatSnackBar,
    private router: Router,
    ){
    this._productosService.route = 'producto';
  }
  ngOnInit(): void {
    this.getProductos();
  }
  addToShoppingList(i : string){
    console.log()
    if(this.sizeProduct[i] !== undefined){
      this.shoppingList.push(this.productos[i])
      this.shoppingList[this.shoppingList.length-1].medida = this.sizeProduct[i];
      this.sizeProduct[i] = undefined;
      console.log(this.shoppingList);
      this.showSnackbar(
        'Producto agregado al carrito.',
        '',
        2000
      );
    }
    else{  
      this.showSnackbar(
        'Favor de introducir una medida.',
        'Aceptar',
        4000
      );
    }    
  }
  navigateShoppingCar(){
    this.router.navigateByUrl('/shopping-cart');
  }
  getProductos(): void {
    this._productosService.list().subscribe(
      ({ producto }) => {
        console.log(producto)
        this.productos = producto;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  showSnackbar(message: string, action: string, ms: number): void {
    this._snackBar.open(message, action, { 
      duration: ms
  });
  }
}
