import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CRUDService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registroForm: FormGroup;
  submitted = false;

  constructor(
    public dialogo: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public titulo: string,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _clienteService: CRUDService
  ) {
    this._clienteService.route = 'cliente';
  }

  ngOnInit(): void {
    this.setupForm();
  }

  setupForm(): void {
    this.registroForm = this.formBuilder.group({
      companyname: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      rfc: [
        '',
        [
          Validators.required,
          Validators.minLength(12),
          Validators.maxLength(13),
        ],
      ],
      calle: ['', [Validators.required]],
      colonia: ['', [Validators.required]],
      codigo_postal: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(6), Validators.required]],
      tycs: [false, [Validators.required]],
    });
  }

  /**
   * En caso de cancelar
   */
  cerrarDialogo(): void {
    this.dialogo.close(false);
  }

  /**
   * En caso de confirmar
   */
  confirmado(): void {
    this._clienteService.save(this.registroForm).subscribe(
      (res) => this.dialogo.close(true),
      (err) => {
        console.log(err);
        this.submitted = true;
        this.showSnackbar(
          'Ocurrió un error al guardar el usuario',
          'Aceptar',
          2000
        );
      }
    );
  }

  showSnackbar(message: string, action: string, options: Object): void {
    this._snackBar.open(message, action, options);
  }
}
