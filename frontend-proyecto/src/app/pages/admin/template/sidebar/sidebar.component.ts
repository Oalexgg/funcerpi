import { MediaMatcher } from '@angular/cdk/layout';
import {
  Component,
  OnDestroy,
  ChangeDetectorRef,
  OnInit,
  Inject,
} from '@angular/core';
import { StorageService } from 'src/app/services/storage/storage.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CRUDService } from 'src/app/services/crud/crud.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [
    DatePipe,
    { provide: 'notificationService', useClass: CRUDService },
  ],
})
export class SidebarComponent implements OnInit, OnDestroy {
  user: any;
  mobileQuery: MediaQueryList;
  selectedPage = 'dashboard';
  fillerNav = [];
  system_notifications = [];

  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private _storageService: StorageService,
    private _authService: AuthService,
    private router: Router,
    public dialog: MatDialog,
    @Inject('notificationService') private notificationService: CRUDService,
    private datePipe: DatePipe
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.notificationService.route = 'pedidos';
  }

  ngOnInit() {
    this.user =
      JSON.parse(localStorage.getItem('user')) || this._authService.cliente;
    this.fillerNav = this.user?.tipo?.permisos.filter(
      (permiso) => permiso.permiso
    );
    this.getNotifications();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

  logout(): void {
    this._storageService.removeItem('user');
    this.router.navigateByUrl('/login');
  }

  getNotifications() {
    this.notificationService
      .list(null, {
        estado: 'No revisado',
      })
      .subscribe((res: any) => {
        const { pedido } = res;
        if (pedido) {
          this.system_notifications = pedido.map(
            (ped) =>
              `Cuentas con un pedido de la fecha ${this.datePipe.transform(
                ped.fecha,
                'mediumDate',
                undefined,
                'es-mx'
              )} por ${ped.productos.length} producto`
          );
        }
      });
  }
}
