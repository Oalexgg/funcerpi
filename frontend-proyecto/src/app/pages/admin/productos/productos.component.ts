import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

import { CRUDService } from 'src/app/services/crud/crud.service';

import { DeviceDetectorService } from 'ngx-device-detector';

import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { AddProductoComponent } from './add-producto/add-producto.component';
import { UploadService } from 'src/app/services/upload/upload.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
})
export class ProductosComponent implements OnInit {
  labels = ['#', 'Nombre', 'Precio'];
  rows = [];
  productos = [];
  productoForm: FormGroup;
  isMobile = false;

  constructor(
    public dialog: MatDialog,
    private _productoService: CRUDService,
    private _snackBar: MatSnackBar,
    private deviceService: DeviceDetectorService,
    private uploadService: UploadService
  ) {
    this._productoService.route = 'producto';
    this.isMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.getProductos();
    this.createForm({});
  }

  getProductos(): void {
    this._productoService.list().subscribe((res) => {
      const { producto } = res;
      this.productos = producto;
      this.rows = producto.map((product: any, idx: number) => [
        idx + 1,
        product.nombre,
        `$${product.precio}`,
      ]);
    });
  }

  createForm({ nombre = '', descripcion = '', precio = '', img = '' }): void {
    this.productoForm = new FormGroup({
      nombre: new FormControl(nombre, [Validators.required]),
      descripcion: new FormControl(descripcion, [Validators.required]),
      precio: new FormControl(precio, [Validators.required]),
      img: new FormControl(img),
      image: new FormControl(null),
    });
  }

  createDialog(edit: boolean): any {
    return this.dialog.open(AddProductoComponent, {
      width: this.isMobile ? '100vw' : '40vw',
      data: {
        form: this.productoForm,
        isEdditing: edit,
      },
    });
  }

  openDialog(): void {
    this.createForm({});
    const dialogRef = this.createDialog(false);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.get('image').value) {
          this.uploadService.upload(result.get('image').value).subscribe(
            (res: any) => {
              result.patchValue({
                img: res.filename,
              });
              result.get('img').updateValueAndValidity();
              this.saveProducto(result);
            },
            (err) => {
              this.showSnackbar(
                'Algo salió mal, por favor revise los datos ingresados.',
                'Aceptar',
                4000
              );
            }
          );
        } else {
          this.saveProducto(result);
        }
      }
    });
  }

  saveProducto(producto: any): void {
    this._productoService.save(producto).subscribe(
      (res) => {
        this.getProductos();
      },
      (err) => {
        this.showSnackbar(
          'Algo salió mal, por favor revise los datos ingresados.',
          'Aceptar',
          4000
        );
      }
    );
  }

  onEdit(ev): void {
    const toEdit = this.productos[ev];
    this.createForm({ ...toEdit });
    const dialogRef = this.createDialog(true);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.openConfirm(
          '¿Estás seguro que deseas editar este registro?'
        ).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            if (result) {
              if (result.get('image').value) {
                this.uploadService.upload(result.get('image').value).subscribe(
                  (res: any) => {
                    result.patchValue({
                      img: res.filename,
                    });
                    result.get('img').updateValueAndValidity();
                    this.editProducto(result, toEdit._id);
                  },
                  (err) => {
                    this.showSnackbar(
                      'Algo salió mal, por favor revise los datos ingresados.',
                      'Aceptar',
                      4000
                    );
                  }
                );
              } else {
                this.editProducto(result, toEdit._id);
              }
            }
          }
        });
      }
    });
  }

  editProducto(producto: any, id: string): void {
    this._productoService.update(producto, id).subscribe(
      (res) => {
        this.showSnackbar('Editado correctamente.', 'Aceptar', 2000);
        this.getProductos();
      },
      (err) =>
        this.showSnackbar(
          'Algo salió mal, por favor revise los datos ingresados.',
          'Aceptar',
          4000
        )
    );
  }

  openConfirm(message): Observable<boolean> {
    return this.dialog
      .open(ConfirmComponent, {
        data: message,
      })
      .afterClosed();
  }

  onDelete(ev): void {
    console.log(ev);
  }

  showSnackbar(message: string, action: string, options: Object): void {
    this._snackBar.open(message, action, options);
  }
}
