import { Component, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { image_url } from 'src/app/shared/constants';
interface DataInterface {
  form: FormGroup;
  isEdditing: boolean;
}

@Component({
  selector: 'app-add-producto',
  templateUrl: './add-producto.component.html',
  styleUrls: ['./add-producto.component.scss'],
})
export class AddProductoComponent {
  isEdditing: boolean;
  productoForm: FormGroup;
  imageURL = '';
  public readonly SERVER_URL = image_url;

  constructor(
    public dialogRef: MatDialogRef<AddProductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataInterface
  ) {
    this.isEdditing = data.isEdditing;
    this.productoForm = data.form;
    const img = data.form.get('img').value;
    if (img) {
      this.imageURL = `${this.SERVER_URL}${img}`;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  showPreview(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.productoForm.patchValue({
      image: file,
    });
    this.productoForm.get('image').updateValueAndValidity();
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  removeImage(): void {
    this.imageURL = '';
    this.productoForm.patchValue({
      image: null,
      img: '',
    });
    this.productoForm.get('image').updateValueAndValidity();
  }
}
