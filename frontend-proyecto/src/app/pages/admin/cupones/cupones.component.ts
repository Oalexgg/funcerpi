import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

import { CRUDService } from 'src/app/services/crud/crud.service';

import { DeviceDetectorService } from 'ngx-device-detector';

import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { AddCuponComponent } from './add-cupon/add-cupon.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-cupones',
  templateUrl: './cupones.component.html',
  styleUrls: ['./cupones.component.scss'],
  providers: [DatePipe],
})
export class CuponesComponent implements OnInit {
  labels = ['#', 'Descripción', 'Código', 'Expira el día', 'Activo'];
  rows = [];
  cupons = [];
  cuponForm: FormGroup;
  isMobile = false;

  constructor(
    public dialog: MatDialog,
    private _cuponService: CRUDService,
    private _snackBar: MatSnackBar,
    private deviceService: DeviceDetectorService,
    private datePipe: DatePipe
  ) {
    this._cuponService.route = 'cupon';
    this.isMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.getCupones();
    this.createForm({});
  }

  getCupones(): void {
    this._cuponService.list().subscribe((res) => {
      const { cupon } = res;
      this.cupons = cupon;
      this.rows = cupon.map((cupon: any, idx: number) => [
        idx + 1,
        cupon.descripcion,
        cupon.codigo,
        this.datePipe.transform(
          cupon.expiracion,
          'mediumDate',
          undefined,
          'es-mx'
        ) || 'N/A',
        cupon.estado ? 'Si' : 'No',
      ]);
    });
  }

  createForm({
    descripcion = '',
    codigo = '',
    estado = true,
    expiracion = new Date(),
  }): void {
    this.cuponForm = new FormGroup({
      descripcion: new FormControl(descripcion, [Validators.required]),
      codigo: new FormControl(codigo, [Validators.required]),
      estado: new FormControl(estado, [Validators.required]),
      expiracion: new FormControl(expiracion, [Validators.required]),
    });
  }

  createDialog(edit: boolean): any {
    return this.dialog.open(AddCuponComponent, {
      width: this.isMobile ? '100vw' : '40vw',
      data: {
        form: this.cuponForm,
        isEdditing: edit,
      },
    });
  }

  openDialog(): void {
    this.createForm({});
    const dialogRef = this.createDialog(false);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this._cuponService.save(result).subscribe((res) => {
          this.getCupones();
        });
      }
    });
  }

  onEdit(ev): void {
    const toEdit = this.cupons[ev];
    this.createForm({ ...toEdit });
    const dialogRef = this.createDialog(true);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.openConfirm(
          '¿Estás seguro que deseas editar este registro?'
        ).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            this._cuponService.update(result, toEdit._id).subscribe(
              (res) => {
                this.showSnackbar('Editado correctamente.', 'Aceptar', 2000);
                this.getCupones();
              },
              (err) =>
                this.showSnackbar(
                  'Algo salió mal, por favor revise los datos ingresados.',
                  'Aceptar',
                  4000
                )
            );
          }
        });
      }
    });
  }

  openConfirm(message): Observable<boolean> {
    return this.dialog
      .open(ConfirmComponent, {
        data: message,
      })
      .afterClosed();
  }

  onDelete(ev): void {
    console.log(ev);
  }

  showSnackbar(message: string, action: string, options: Object): void {
    this._snackBar.open(message, action, options);
  }
}
