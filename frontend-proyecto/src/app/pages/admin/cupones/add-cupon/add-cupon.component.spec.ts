import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCuponComponent } from './add-cupon.component';

describe('AddCuponComponent', () => {
  let component: AddCuponComponent;
  let fixture: ComponentFixture<AddCuponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCuponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCuponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
