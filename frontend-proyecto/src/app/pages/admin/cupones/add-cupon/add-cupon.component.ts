import { Component, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
interface DataInterface {
  form: FormGroup;
  isEdditing: boolean;
}

@Component({
  selector: 'app-add-cupon',
  templateUrl: './add-cupon.component.html',
  styleUrls: ['./add-cupon.component.scss'],
})
export class AddCuponComponent {
  isEdditing: boolean;
  cuponForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddCuponComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataInterface
  ) {
    this.isEdditing = data.isEdditing;
    this.cuponForm = data.form;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  generarCodigo(): void {
    const val = Math.random().toString(16).substring(2, 6) +
      Math.random().toString(16).substring(2, 6);
    this.cuponForm.patchValue({
      codigo: val,
    });
    this.cuponForm.get('codigo').updateValueAndValidity();
  }
}
