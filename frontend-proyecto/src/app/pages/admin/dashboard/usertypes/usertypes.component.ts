import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { permisos } from 'src/app/shared/constants';

import { CRUDService } from 'src/app/services/crud/crud.service';

import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { AddUsertypeComponent } from './add-usertype/add-usertype.component';

import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-usertypes',
  templateUrl: './usertypes.component.html',
  styleUrls: ['./usertypes.component.scss'],
})
export class UsertypesComponent implements OnInit {
  labels = ['Nombre'];
  rows = [];
  user_types = [];
  tipoUsuarioForm: FormGroup;
  permisos = permisos;
  isMobile = false;

  constructor(
    public dialog: MatDialog,
    private _userTypesService: CRUDService,
    private deviceService: DeviceDetectorService
  ) {
    this._userTypesService.route = 'usertype';
    this.isMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.getTipoUsuarios();
    this.createForm({});
  }

  getTipoUsuarios(): void {
    this._userTypesService.list().subscribe(
      (res) => {
        if (res.error) {
          return;
        }
        const { usertype } = res;
        this.user_types = usertype;
        this.rows = usertype.map((user: any) => [user.nombre]);
      },
      (err) => console.log(err)
    );
  }

  createForm({ permisos = this.permisos }): void {
    this.tipoUsuarioForm = new FormGroup({
      permisos: new FormControl(permisos, [Validators.required]),
    });
  }

  createDialog(edit: boolean, permisos): any {
    const perms = this.permisos.map((permiso) => ({
      ...permiso,
      permiso: permisos.find((permi) => permi.text === permiso.text)?.permiso,
    }));
    return this.dialog.open(AddUsertypeComponent, {
      width: this.isMobile ? '100vw' : '40vw',
      data: {
        form: this.tipoUsuarioForm,
        isEdditing: edit,
        permisos: perms,
      },
    });
  }

  onEdit(ev): void {
    const toEdit = this.user_types[ev];
    this.createForm({ ...toEdit });
    const dialogRef = this.createDialog(true, toEdit.permisos);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.openConfirm(
          '¿Estás seguro que deseas editar este registro?'
        ).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            this._userTypesService
              .update({ value: { permisos: result } }, toEdit._id)
              .subscribe((res) => {
                this.getTipoUsuarios();
              });
          }
        });
      }
    });
  }

  openConfirm(message): Observable<boolean> {
    return this.dialog
      .open(ConfirmComponent, {
        data: message,
      })
      .afterClosed();
  }

  onDelete(ev): void {
    const toDelete = this.user_types[ev];
    this.openConfirm(
      '¿Estás seguro que deseas eliminar este registro?'
    ).subscribe((confirmado: Boolean) => {
      if (confirmado) {
        this._userTypesService.delete(toDelete._id).subscribe((res) => {
          this.getTipoUsuarios();
        });
      }
    });
  }
}
