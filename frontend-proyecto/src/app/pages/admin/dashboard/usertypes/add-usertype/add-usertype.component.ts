import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
interface DataInterface {
  form: FormGroup;
  isEdditing: boolean;
  permisos: Array<any>;
}

@Component({
  selector: 'app-add-usertype',
  templateUrl: './add-usertype.component.html',
  styleUrls: ['./add-usertype.component.scss'],
})
export class AddUsertypeComponent {
  isEdditing: boolean;
  userTypeForm: FormGroup;
  actives_types = [];
  permisos: Array<any>;

  constructor(
    public dialogRef: MatDialogRef<AddUsertypeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataInterface
  ) {
    this.isEdditing = data.isEdditing;
    this.userTypeForm = data.form;
    this.permisos = data.permisos;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
