import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';

import { CRUDService } from 'src/app/services/crud/crud.service';

import { DeviceDetectorService } from 'ngx-device-detector';

import { ConfirmComponent } from 'src/app/components/confirm/confirm.component';
import { AddUserComponent } from './add-user/add-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [
    { provide: 'UsersService', useClass: CRUDService },
    { provide: 'UserTypesService', useClass: CRUDService },
  ],
})
export class UsersComponent implements OnInit {
  labels = ['#', 'Nombre', 'Empresa'];
  rows = [];
  users = [];
  userForm: FormGroup;
  userTypes: any; //El tipo será array de user_type una vez creado el modelo
  isMobile = false;

  constructor(
    public dialog: MatDialog,
    @Inject('UsersService') private _usersService: CRUDService,
    @Inject('UserTypesService') private _userTypesService: CRUDService,
    private _snackBar: MatSnackBar,
    private deviceService: DeviceDetectorService
  ) {
    this._usersService.route = 'cliente';
    this._userTypesService.route = 'usertype';
    this.isMobile = this.deviceService.isMobile();
  }

  ngOnInit(): void {
    this.getUsuarios();
    this.getTipoUsuarios();
    this.createForm({});
  }

  getUsuarios(): void {
    this._usersService.list().subscribe((res) => {
      const { cliente } = res;
      this.users = cliente;
      this.rows = cliente.map((usuario: any, idx: number) => [
        idx + 1,
        `${usuario.nombre} ${usuario.apellido}`,
        usuario.empresa?.nombre,
      ]);
    });
  }

  getTipoUsuarios(): void {
    this._userTypesService.list().subscribe((res) => {
      const { usertype } = res;
      this.userTypes = usertype;
    });
  }

  createForm({
    nombre = '',
    apellido = '',
    email = '',
    tipo = '',
    companyname = '',
    rfc = '',
    calle = '',
    colonia = '',
    codigo_postal = '',
    telefono = '',
  }): void {
    this.userForm = new FormGroup({
      nombre: new FormControl(nombre, [Validators.required]),
      apellido: new FormControl(apellido, [Validators.required]),
      email: new FormControl(email, [Validators.email, Validators.required]),
      password: new FormControl('******', [
        Validators.minLength(6),
        Validators.required,
      ]),
      tipo: new FormControl(tipo, [Validators.required]),
      companyname: new FormControl(companyname, [Validators.required]),
      rfc: new FormControl(rfc, [
        Validators.required,
        Validators.minLength(12),
        Validators.maxLength(13),
      ]),
      calle: new FormControl(calle, [Validators.required]),
      colonia: new FormControl(colonia, [Validators.required]),
      codigo_postal: new FormControl(codigo_postal, [Validators.required]),
      telefono: new FormControl(telefono, [Validators.required]),
    });
  }

  createDialog(edit: boolean): any {
    return this.dialog.open(AddUserComponent, {
      width: this.isMobile ? '100vw' : '40vw',
      data: {
        form: this.userForm,
        types: this.userTypes,
        isEdditing: edit,
      },
    });
  }

  openDialog(): void {
    this.createForm({});
    const dialogRef = this.createDialog(false);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this._usersService.save(result).subscribe((res) => {
          this.getUsuarios();
        });
      }
    });
  }

  onEdit(ev): void {
    const { _id, nombre, apellido, email, password } = this.users[ev];
    const {
      nombre: companyname,
      rfc,
      calle,
      colonia,
      codigo_postal,
      telefono,
    } = this.users[ev].empresa;
    const { _id: tipo } = this.users[ev].tipo;
    this.createForm({
      nombre,
      apellido,
      email,
      companyname,
      rfc,
      calle,
      colonia,
      codigo_postal,
      telefono,
      tipo,
    });
    const dialogRef = this.createDialog(true);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.openConfirm(
          '¿Estás seguro que deseas editar este registro?'
        ).subscribe((confirmado: Boolean) => {
          if (confirmado) {
            this._usersService.update(result, _id).subscribe(
              (res) => {
                this.showSnackbar('Editado correctamente.', 'Aceptar', 2000);
                this.getUsuarios();
              },
              (err) =>
                this.showSnackbar(
                  'Algo salió mal, por favor revise los datos ingresados.',
                  'Aceptar',
                  4000
                )
            );
          }
        });
      }
    });
  }

  openConfirm(message): Observable<boolean> {
    return this.dialog
      .open(ConfirmComponent, {
        data: message,
      })
      .afterClosed();
  }

  onDelete(ev): void {
    const { _id } = this.users[ev];
    this.openConfirm(
      '¿Estás seguro que deseas eliminar este registro?'
    ).subscribe((confirmado: Boolean) => {
      if (confirmado) {
        this._usersService.delete(_id).subscribe(
          (res) => {
            this.showSnackbar('Eliminado correctamente.', 'Aceptar', 2000);
            this.getUsuarios();
          },
          (err) =>
            this.showSnackbar(
              'Algo salió mal, por favor revise los datos ingresados.',
              'Aceptar',
              4000
            )
        );
      }
    });
  }

  showSnackbar(message: string, action: string, options: Object): void {
    this._snackBar.open(message, action, options);
  }
}
