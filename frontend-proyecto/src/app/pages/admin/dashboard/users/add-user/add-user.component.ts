import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
interface DataInterface {
  form: FormGroup;
  isEdditing: boolean;
  types: Array<any>;
}

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent {
  isEdditing: boolean;
  userForm: FormGroup;
  user_types = [];
  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataInterface
  ) {
    this.isEdditing = data.isEdditing;
    this.userForm = data.form;
    this.user_types = data.types;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
