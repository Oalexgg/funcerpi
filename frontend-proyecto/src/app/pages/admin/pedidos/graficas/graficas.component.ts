import { Component, OnInit } from '@angular/core';
import { CRUDService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.scss'],
})
export class GraficasComponent implements OnInit {
  single =[{
    "name": "Germany",
    "value": 8940000
  },
  {
    "name": "USA",
    "value": 5000000
  },
  {
    "name": "France",
    "value": 7200000
  }];
  
  // options
  opcionesGraficaPedidosMensuales = {
    showXAxis: true,
    showYAxis: true,
    gradient: false,
    showLegend: false,
    showXAxisLabel: true,
    xAxisLabel: 'Cantidad',
    showYAxisLabel: true,
    yAxisLabel: 'Mes',
  };

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'],
  };

  constructor(private pedidosService: CRUDService) {
    this.pedidosService.route = 'pedidos';
  }

  ngOnInit(): void {}

  getPedidos(): void {
    this.pedidosService.list().subscribe(
      (res) => {
        const { pedido } = res;
        console.log(pedido);
      },
      (err) => console.log(err)
    );
  }

  onSelect(event) {
    console.log(event);
  }
}
