import { Component, OnInit } from '@angular/core';
import { CRUDService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.scss'],
})
export class PendientesComponent implements OnInit {
  rows = [];
  labels = [
    'Fecha',
    'Total estimado',
    'Productos',
    'Cupón',
    'Cliente',
    'Estado',
  ];
  filters = false;

  constructor(private pedidosService: CRUDService) {
    this.pedidosService.route = 'pedido';
  }

  ngOnInit(): void {
    this.getPedidos();
  }

  getPedidos(): void {
    this.pedidosService.list().subscribe(
      (res) => {
        const { pedido } = res;
        this.rows = pedido.map((ped) => [
          ped.fecha,
          ped.total,
          ped.productos.length,
          ped.cupon || 'N/A',
          ped.cliente.nombre || 'N/A',
          ped.estado,
        ]);
      },
      (err) => console.log(err)
    );
  }

  onView(event: Event): void {
    console.log(event);
  }
}
