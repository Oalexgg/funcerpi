import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLoginGuard } from 'src/app/guards/admin/admin.guard';
import { AdminComponent } from './admin.component';
import { CuponesComponent } from './cupones/cupones.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ProductosComponent } from './productos/productos.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminLoginGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AdminLoginGuard],
      },
      {
        path: 'productos',
        component: ProductosComponent,
        canActivate: [AdminLoginGuard],
      },
      {
        path: 'cupones',
        component: CuponesComponent,
        canActivate: [AdminLoginGuard],
      },
      {
        path: 'pedidos',
        component: PedidosComponent,
        canActivate: [AdminLoginGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
