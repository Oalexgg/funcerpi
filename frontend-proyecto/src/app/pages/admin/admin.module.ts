import { NgModule } from '@angular/core';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SidebarComponent } from './template/sidebar/sidebar.component';
import { HeaderComponent } from './template/header/header.component';
import { FooterComponent } from './template/footer/footer.component';

import { MaterialModule } from './template/material.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductosComponent } from './productos/productos.component';
import { FabComponent } from './../../components/fab/fab.component';
import { TablesComponent } from './../../components/tables/tables.component';
import { ConfirmComponent } from './../../components/confirm/confirm.component';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { SafeHtmlPipe } from 'src/app/pipes/safe-html.pipe';
import { AddProductoComponent } from './productos/add-producto/add-producto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './dashboard/users/users.component';
import { UsertypesComponent } from './dashboard/usertypes/usertypes.component';
import { AddUserComponent } from './dashboard/users/add-user/add-user.component';
import { AddUsertypeComponent } from './dashboard/usertypes/add-usertype/add-usertype.component';
import { CuponesComponent } from './cupones/cupones.component';
import { AddCuponComponent } from './cupones/add-cupon/add-cupon.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { GraficasComponent } from './pedidos/graficas/graficas.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TablasComponent } from './pedidos/tablas/tablas.component';
import { PendientesComponent } from './pedidos/pendientes/pendientes.component';

@NgModule({
  declarations: [
    AdminComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    ProductosComponent,
    FabComponent,
    TablesComponent,
    ConfirmComponent,
    SafeHtmlPipe,
    AddProductoComponent,
    UsersComponent,
    UsertypesComponent,
    AddUserComponent,
    AddUsertypeComponent,
    CuponesComponent,
    AddCuponComponent,
    PedidosComponent,
    GraficasComponent,
    TablasComponent,
    PendientesComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    DataTablesModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
  ],
  providers: [],
})
export class AdminModule {}
