import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
})
export class FabComponent implements OnInit {
  @Output() onClick = new EventEmitter<any>();
  constructor() {}

  ngOnInit(): void {}

  clicked() {
    this.onClick.emit('');
  }
}
