import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  constructor(
    public dialogo: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) public titulo: string
  ) {}

  /**
   * En caso de cancelar
   */
  cerrarDialogo(): void {
    this.dialogo.close(false);
  }

  /**
   * En caso de confirmar
   */
  confirmado(): void {
    this.dialogo.close(true);
  }
  ngOnInit(): void {}
}
