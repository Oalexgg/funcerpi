import {
  Component,
  OnDestroy,
  Input,
  IterableDiffers,
  DoCheck,
  ViewChild,
  Output,
  EventEmitter
} from "@angular/core";
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
const language = {
  decimal: "",
  emptyTable: "No hay datos disponibles en la tabla",
  info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
  infoEmpty: "Mostrando del 0 al 0 de 0 registros",
  infoFiltered: "(Filtrado desde _MAX_ registros totales)",
  infoPostFix: "",
  thousands: ",",
  lengthMenu: "Mostrar _MENU_ registros",
  loadingRecords: "Cargando...",
  processing: "Procesando...",
  search: "Buscar:",
  zeroRecords: "No se encontraron registros",
  paginate: {
    first: "Primer",
    last: "Último",
    next: "Siguiente",
    previous: "Anterior"
  },
  aria: {
    sortAscending: ": activar para ordenar columna ascendente",
    sortDescending: ": activar para ordenar columna descendente"
  }
};

@Component({
  selector: "app-tables",
  templateUrl: "./tables.component.html",
  styleUrls: ["./tables.component.scss"]
})
export class TablesComponent implements OnDestroy, DoCheck {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  differ: any;
  first: Boolean = true;
  empty: Boolean = true;

  @Input("labels") labels_inp: string[];
  @Input("rows") rows_inp: string[][];
  @Input("cblabels") cblabels_inp: string[];
  @Input("botones") botones_inp: string[];
  @Input() edit: Boolean = false;
  // agregar input y output
  @Input() delete: Boolean = false;
  @Input() buttons: Boolean = false;
  @Input() customButtons: Boolean = false;

  labels: string[];
  rows: string[][];
  cblabels: string[];
  botones: string[];

  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  @Output() editClick: EventEmitter<number> = new EventEmitter();
  @Output() deleteClick: EventEmitter<number> = new EventEmitter();
  @Output() customEventClick: EventEmitter<any> = new EventEmitter();

  constructor(private _iterableDiffers: IterableDiffers) {
    this.differ = this._iterableDiffers.find([]).create(null);
    this.dtOptions = {
      pageLength: 20,
      order: [],
      columnDefs: [{ orderable: false, targets: "no-sort" }],
      language: language
    };
  }

  ngDoCheck() {
    const change = this.differ.diff(this.rows_inp);
    this.labels = this.labels_inp;
    if (change) {
      if (this.rows_inp.length > 0) {
        this.empty = false;
        if (this.buttons) {
          this.dtOptions.dom = "Bfrtip";
          this.dtOptions.buttons = ["copy", "print", "excel", "csv"];
        }
        if (this.customButtons) {
          this.cblabels = this.cblabels_inp;
          this.botones = this.botones_inp;
        }
        this.rerender();
        this.first = false;
      } else {
        this.rerender();
      }
    }
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    if (!this.first) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.rows = [];
      });
    }
    setTimeout(() => {
      this.rows = this.rows_inp;
      this.dtTrigger.next();
    }, 50);
  }

  onEdit(index: number) {
    this.editClick.emit(index);
  }

  onDelete(index: number) {
    this.deleteClick.emit(index);
  }

  customEvt(evento: string, index: number) {
    this.customEventClick.emit({ event: evento, index: index });
  }
}

// Este componente fue sacado de:
// https://l-lin.github.io/angular-datatables/
// Este componente manda a llamar Datatables para angular consta de auto render cada que se cambian los datos y arregla
// problemas de rerenderizado

// Uso:
//   <app-tables
//       [labels]="labels"                   Los titulos de las columnas en un arreglo
//       [rows]="rows"                       Los datos de la tabla en un arreglo de arreglos cada uno equivale a una fila
//       (editClick)="onEdit($event)"        Se regresa el index del arreglo que se quiere editar
//       (deleteClick)="onDelete($event)"    Se regresa el index del arreglo que se quiere eliminar
//       [buttons]="true"                    Se activan los botones de copiar, imprimir, excel y csv
//       [edit]="true"                       Activa la icono de editar
//       [delete]="true"                     Activa icono de eliminar
//       [customButtons]="false"             Activa el añadir botones personalizados
//       [cblabels]="labelsBotones"          Títulos de los botones
//       [botones]="botones">                Esta propiedad incluye la clase de cada botón, ejemplo: mdi mdi-delete-empty text-danger pointer
//   </app-tables>

// ______________________ Nota de manejo de eventos ___________________
// para manejar un evento custom se necesita enviar el nombre del evento por medio el even emitter, el cual será enviado en base al nombre que se le de en cblabels,
// para después manejarlo en el componente necesario, ejemplo:
// customEvents(evento: any) { --Recibimos el evento, el cual es un objeto con el nombre del evento e index, el index se manda como complemento en caso que lo requieran --
//   if (evento.event.toLowerCase() == 'imprimir') { -- Aquí hacemos el manejo del evento, en este caso es cuando se da click a imprimir se manda al método imprimir
//     this.imprimir(evento.index);
//   }
// }
// ______________________Fin nota de manejo de eventos ___________________

// ng add angular-datatables

// npm install jszip --save
// npm install datatables.net-buttons --save
// npm install datatables.net-buttons-dt --save
// npm install @types/datatables.net-buttons --save-dev

// Importaciones a Angular.json
// "styles": [
//   "node_modules/datatables.net-dt/css/jquery.dataTables.css",
//   "node_modules/datatables.net-buttons-dt/css/buttons.dataTables.css"
// ],
//   "scripts": [
//     "node_modules/jquery/dist/jquery.js",
//     "node_modules/datatables.net/js/jquery.dataTables.js",
//     "node_modules/jszip/dist/jszip.js",
//     "node_modules/datatables.net-buttons/js/dataTables.buttons.js",
//     "node_modules/datatables.net-buttons/js/buttons.colVis.js",
//     "node_modules/datatables.net-buttons/js/buttons.flash.js",
//     "node_modules/datatables.net-buttons/js/buttons.html5.js",
//     "node_modules/datatables.net-buttons/js/buttons.print.js"
//   ]
