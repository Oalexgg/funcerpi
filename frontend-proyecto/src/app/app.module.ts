import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LandingComponent } from './pages/cliente/landing-page/landing.component';
import { HomeComponent } from './pages/cliente/home/home.component';
import { RegisterComponent } from './pages/cliente/register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { CatalogueComponent } from './pages/cliente/catalogue/catalogue.component';
import { LoginComponent } from './pages/cliente/login/login.component';

import { AdminModule } from './pages/admin/admin.module';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ModalComponent } from './components/modal/modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { CRUDService } from './services/crud/crud.service';
import { LandingBaseComponent } from './pages/cliente/landing-base/landing-base.component';
import { MatSelectModule } from '@angular/material/select';
import { MaterialModule } from './pages/admin/template/material.module';
import { UploadService } from './services/upload/upload.service';
import { ShoppingCarComponent } from './pages/cliente/shopping-car/shopping-car.component';

import { registerLocaleData } from '@angular/common';
import localeMx from '@angular/common/locales/es-MX';
registerLocaleData(localeMx);

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    HomeComponent,
    LoginComponent,
    CatalogueComponent,
    RegisterComponent,
    NotFoundComponent,
    ModalComponent,
    LandingBaseComponent,
    ShoppingCarComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AdminModule,
    MatSnackBarModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatSelectModule,
    MaterialModule,
  ],
  providers: [
    CRUDService,
    UploadService,
    { provide: LOCALE_ID, useValue: 'es-MX' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
