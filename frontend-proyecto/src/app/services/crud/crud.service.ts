import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { url } from '../../shared/constants';
import { FormGroup } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class CRUDService {
  private url = url;
  public route = '';
  constructor(private httpClient: HttpClient) {}

  list(id?: string, params = {}): Observable<any> {
    return this.httpClient.get(`${this.url}${this.route}/${id ? id : ''}`, {
      params,
    });
  }

  save(data: FormGroup): Observable<any> {
    const { value } = data;
    return this.httpClient.post(`${this.url}${this.route}`, value, {});
  }

  update(data: any, id: string): Observable<any> {
    const { value } = data;
    return this.httpClient.put(`${this.url}${this.route}/${id}`, value, {});
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(`${this.url}${this.route}/${id}`);
  }
}
