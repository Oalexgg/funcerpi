import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { url } from '../../shared/constants';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  private readonly SERVER_URL = `${url}upload`;
  constructor(private httpClient: HttpClient) {}

  public upload(image: File) {
    console.log(image)
    const formData = new FormData();
    formData.append('file', image);
    return this.httpClient.post(this.SERVER_URL, formData, {});
  }
}
