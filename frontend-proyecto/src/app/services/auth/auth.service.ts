import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { url } from '../../shared/constants';
import { StorageService } from '../storage/storage.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private url = url;
  isLoggedInUser: boolean;
  isLoggedInAdmin: boolean;
  public cliente: any;

  constructor(
    private httpClient: HttpClient,
    private storageService: StorageService
  ) {
    //Watches the first time
    this.isLoggedInAdmin = this.checkType('admin');
    this.isLoggedInUser = this.checkType('user');
    this.storageService.watchStorage().subscribe((data: string) => {
      //Watches on changes
      this.isLoggedInAdmin = this.checkType('admin');
      this.isLoggedInUser = this.checkType('user');
    });
  }

  login({ email, password }): Observable<any> {
    return this.httpClient.post(`${this.url}login`, { email, password });
  }

  checkType(type: string): boolean {
    return JSON.parse(localStorage.getItem('user'))?.tipo?.nombre === type;
  }
}
