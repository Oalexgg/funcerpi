export const url = 'http://localhost:3000/api/';
export const image_url = 'http://localhost:3000/images/';
// export const url = 'http://192.168.100.3:3000/api/';
// export const image_url = 'http://192.168.100.3:3000/images/';

export const permisos = [
  {
    text: 'Panel de control',
    route: '/admin/dashboard',
    icon: 'dashboard',
    permiso: false,
  },
  {
    text: 'Productos',
    route: '/admin/productos',
    icon: 'login',
    permiso: false,
  },
  {
    text: 'Cupones',
    route: '/admin/cupones',
    icon: 'card_giftcard',
    permiso: false,
  },
  {
    text: 'Pedidos',
    route: '/admin/pedidos',
    icon: 'store',
    permiso: false,
  },
];
