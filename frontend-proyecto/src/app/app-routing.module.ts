import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './pages/cliente/landing-page/landing.component';
import { CatalogueComponent } from './pages/cliente/catalogue/catalogue.component';
import { ShoppingCarComponent } from './pages/cliente/shopping-car/shopping-car.component';
import { LoginComponent } from './pages/cliente/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./pages/admin/admin.module').then((m) => m.AdminModule),
  },
  { path: 'login', component: LoginComponent, },
  { path: 'catalogo', component: CatalogueComponent },
  { path: 'shopping-cart', component: ShoppingCarComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
